﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowState : IState
{

    private Enemy enemy;

    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void Exit()
    {
        enemy.Direction = Vector2.zero;
    }

    public void Execute()
    {
        if (enemy.Target != null)
        {
            enemy.Direction = (enemy.Target.position - enemy.transform.position).normalized;
            enemy.transform.position = Vector2.MoveTowards(enemy.transform.position, enemy.Target.position, enemy.Speed * Time.deltaTime);

            float distance = Vector2.Distance(enemy.Target.position, enemy.transform.position);

            if (distance <= enemy.MyAttackRange)
            {
                enemy.ChangeState(new AttackState());
            }
        }
        if(!enemy.InRange)
        {
            enemy.ChangeState(new EvadeState());
        }
    }
}
