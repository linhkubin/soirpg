﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IState
{
    private Enemy enemy;

    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
        this.enemy.Reset();
    }

    public void Exit()
    {
        
    }

    public void Execute()
    {
        if (enemy.Target != null)
        {
            enemy.ChangeState(new FollowState());
        }
    }
}
