﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvadeState : IState
{

    private Enemy enemy;

    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void Execute()
    {
        enemy.Direction = (enemy.StartPosition - enemy.transform.position).normalized;
        enemy.transform.position = Vector2.MoveTowards(enemy.transform.position, enemy.StartPosition, enemy.Speed * Time.deltaTime);

        float distance = Vector2.Distance(enemy.StartPosition, enemy.transform.position);

        if (distance <= 0)
        {
            enemy.ChangeState(new IdleState());
        }
    }

    public void Exit()
    {
        enemy.Direction = Vector2.zero;
        enemy.Reset();
    }

}
