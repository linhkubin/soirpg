﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IState
{

    private Enemy enemy;

    private float attackCooldown = 3;

    private float extraRange = 0.1f;

    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void Exit()
    {

    }

    public void Execute()
    {
        if (enemy.MyAttackTime >= attackCooldown && !enemy.IsAttacking)
        {
            enemy.MyAttackTime = 0;
            enemy.StartCoroutine(Attack());
        }

        if (enemy.Target != null)
        {
            float distance = Vector2.Distance(enemy.Target.position, enemy.transform.position);

            if (distance >= enemy.MyAttackRange +extraRange &&!enemy.IsAttacking)
            {
                enemy.ChangeState(new FollowState());
            }
        }
        else
        {
            enemy.ChangeState(new IdleState());
        }
    }

    public IEnumerator Attack()
    {
        enemy.IsAttacking = true;

        enemy.MyAnimator.SetTrigger("attack");

        yield return new WaitForSeconds(enemy.MyAnimator.GetCurrentAnimatorStateInfo(2).length);

        enemy.IsAttacking = false;
    }
}
