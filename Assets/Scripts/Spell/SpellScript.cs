﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellScript : MonoBehaviour
{
    private Rigidbody2D myRigidbody;

    [SerializeField]
    private float speed;

    public Transform MyTarget { get; set; }

    private int damage;

    private Transform source;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        //MyTarget = GameObject.Find("Target").transform;
    }

    public void Initialize(Transform target, int damage, Transform source)
    {
        this.MyTarget = target;
        this.damage = damage;
        this.source = source;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (MyTarget != null)
        {

            Vector2 direction = MyTarget.position - transform.position;

            myRigidbody.velocity = direction.normalized * speed;

            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "HitBox" && other.transform == MyTarget)
        {
            speed = 0;
            // goi func takedame trong component cha cua hit box
            other.GetComponentInParent<Enemy>().TakeDame(damage, source);

            GetComponent<Animator>().SetTrigger("impact");

            myRigidbody.velocity = Vector2.zero;

            MyTarget = null;
        }
    }

}
