﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Spell : IUseable, IMoveable
{
    [SerializeField]
    private string name;

    public string Name
    {
        get { return name; }
    }

    [SerializeField]
    private int damage;

    public int Damage
    {
        get { return damage; }
    }

    [SerializeField]
    private Sprite icon;

    public Sprite MyIcon
    {
        get { return icon; }
    }

    [SerializeField]
    private float speed;

    public float Speed
    {
        get { return speed; }
    }

    [SerializeField]
    private float castTime;

    public float CastTime
    {
        get { return castTime; }
    }

    [SerializeField]
    private GameObject spellPrefab;

    public GameObject SpellPrefab
    {
        get { return spellPrefab; }
    }

    [SerializeField]
    private Color barColor;

    public Color BarColor
    {
        get { return barColor; }
    }

    public void Use()
    {
        Player.Instance.CastSpell(Name);
    }
}
