﻿using System;//-> serializable
using UnityEngine;

[Serializable]
public class Block 
{
    [SerializeField]
    private GameObject first, second;

    public void Activate()
    {
        first.SetActive(true);
        second.SetActive(true);
    }

    public void Deactivate()
    {
        first.SetActive(false);
        second.SetActive(false);
    }
}
