﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public abstract class Character : MonoBehaviour
{
    [SerializeField]
    private float speed = 0;

    protected Vector2 direction;

    public Animator MyAnimator { get; set; }

    protected Rigidbody2D myRigidbody;

    public bool IsMoving
    {
        get
        {
            return Direction.x != 0 || Direction.y != 0;
        }
    }

    public bool IsAttacking { get; set; }

    protected Coroutine attackRoutine;

    [SerializeField]
    protected Transform hitBox;

    //------------------health---------------------
    [SerializeField]
    protected Stat health;

    public Stat Health { get { return health; } }

    public float Speed { get => speed; set => speed = value; }
    public Vector2 Direction { get => direction; set => direction = value; }

    [SerializeField]
    private float initHeal = 50;

    [SerializeField]
    private float maxHeal = 100;

    public Transform Target { get; set; }
    //---------------------------------------------

    // Start is called before the first frame update
    protected virtual void Start()
    {
        health.Initialize(initHeal, maxHeal);

        MyAnimator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //Move();   
    }

    private void FixedUpdate()
    {
        Move();
        HandleLayers();
    }


    public bool IsAlive
    {
        get
        {
            return Health.CurrentValue > 0;
        }
    }

    public void Move()
    {
        if (IsAlive)
        {
            myRigidbody.velocity = Direction.normalized * speed;

        }

        //transform.Translate(direction * speed * Time.deltaTime);
    }

    public void HandleLayers()
    {
        if (IsAlive)
        {
            if (IsMoving)
            {
                ActivateLayer("WalkLayer");
                MyAnimator.SetFloat("x", Direction.x);
                MyAnimator.SetFloat("y", Direction.y);

            }
            else
            if (IsAttacking)
            {
                ActivateLayer("AttackLayer");
            }
            else
            {
                ActivateLayer("IdleLayer");
            }
        }
        else
        {
            ActivateLayer("DeathLayer");
        }
            
    }

    public void ActivateLayer(string layerName)
    {
        for (int i = 0; i < MyAnimator.layerCount; i++)
        {
            MyAnimator.SetLayerWeight(i, 0);
        }

        MyAnimator.SetLayerWeight(MyAnimator.GetLayerIndex(layerName), 1);
    }

    public virtual void TakeDame(float damage, Transform source)
    {
        health.CurrentValue -= damage;

        if (health.CurrentValue <= 0)
        {
            Direction = Vector2.zero;
            myRigidbody.velocity = Direction;
            MyAnimator.SetTrigger("die");

        }
    }
}
