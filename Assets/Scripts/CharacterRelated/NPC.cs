﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//event change heal
public delegate void HealthChanged(float health);

public delegate void CharacterRemoved();

public class NPC : Character
{
    public event HealthChanged healthChanged;

    public event CharacterRemoved characterRemoved;

    [SerializeField]
    private Sprite portrait;

    public Sprite Portrait { get { return portrait; } }

    public virtual void DeSelect()
    {
        healthChanged -= new HealthChanged(UIManager.Instance.UpdateEnemyFrame);

        characterRemoved -= new CharacterRemoved(UIManager.Instance.HideEnemyFrame);
    }

    public virtual Transform Select()
    {
        return hitBox;
    }

    public void OnHealthChanged(float health)
    {
        if (healthChanged != null)
        {
            healthChanged(health);
        }
    }

    public void OnCharacterRemoved()
    {
        if (characterRemoved != null)
        {
            characterRemoved();
        }

        Destroy(gameObject);
    }
}
