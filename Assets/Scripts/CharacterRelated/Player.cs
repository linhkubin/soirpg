﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player :Character
{
    private static Player instance;

    public static Player Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Player>();
            }

            return instance;
        }
    }

    [SerializeField]
    private float initMana = 50;

    [SerializeField]
    private float maxMana = 100;

    [SerializeField]
    private Stat mana;

    [SerializeField]
    private Block[] blocks;

    [SerializeField]
    private GameObject[] spellPrefab;

    [SerializeField]
    private GameObject[] skillPoint;

    private int skillIndex = 0;

    public Vector3 min, max;

    // Start is called before the first frame update
    protected override void Start()
    {
        mana.Initialize(initMana, maxMana);

        base.Start();

        health.CurrentValue -= 10;


        //MyTarget = GameObject.Find("Target").transform;
    }

    // Update is called once per frame
    protected override void Update()
    {
        GetInput();
        base.Update();

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, min.x, max.x), Mathf.Clamp(transform.position.y, min.y, max.y), transform.position.z); 
    }

    private void GetInput()
    {
        Direction = Vector2.zero;

        if (Input.GetKeyDown(KeyCode.I))
        {
            health.CurrentValue -= 10;
        }

        if (Input.GetKey(KeybindManager.Instance.Keybinds["DOWN"]))
        {
            skillIndex = 0;
            Direction += Vector2.down;
        }
        if (Input.GetKey(KeybindManager.Instance.Keybinds["UP"]))
        {
            skillIndex = 1;
            Direction += Vector2.up;
        }
        if (Input.GetKey(KeybindManager.Instance.Keybinds["RIGHT"]))
        {
            skillIndex = 2;
            Direction += Vector2.right;
        }
        if (Input.GetKey(KeybindManager.Instance.Keybinds["LEFT"]))
        {
            skillIndex = 3;
            Direction += Vector2.left;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            health.CurrentValue += 10;
        }

        if (IsMoving)
        {
            StopAttack();
        }


        foreach(string action in KeybindManager.Instance.ActionBinds.Keys)
        {
            if (Input.GetKeyDown(KeybindManager.Instance.ActionBinds[action]))
            {
                UIManager.Instance.ClickActionButton(action);
            }
        }

    }

    //attack skill
    private IEnumerator Attack(string spellIndex)
    {
        //--> dont change target when click
        Transform currentTarget = Target;

        Spell sb = SpellBook.Instance.CastSpell(spellIndex);

        MyAnimator.SetBool("attack", true);
        IsAttacking = true;

        yield return new WaitForSeconds(sb.CastTime);

        //CastSpell();

        if (currentTarget != null && InLineSight())
        {
            SpellScript s = Instantiate(sb.SpellPrefab, skillPoint[skillIndex].transform.position, Quaternion.identity).GetComponent<SpellScript>();
            // target
            //s.MyTarget = currentTarget;

            s.Initialize(currentTarget, sb.Damage, transform);
        }

        StopAttack();
        Debug.Log("attack");
    }

    // skill
    public void CastSpell(string spellIndex)
    {
        Block();

        if (Target != null && Target.GetComponentInParent<Character>().IsAlive)
        {
            if (!IsAttacking)
            {
                if (!IsMoving)
                {
                    if (InLineSight())
                    {
                        attackRoutine = StartCoroutine(Attack(spellIndex));
                    }
                }
            }
        }

        
    }

    public void SetLimits(Vector3 min, Vector3 max)
    {
        this.min = min;
        this.max = max;
    }

    private bool InLineSight()
    {
        if (Target != null)
        {
            Vector3 targetDirection = (Target.transform.position - transform.position).normalized;

            //Debug.DrawRay(transform.position, targetDirection, Color.red);


            //Debug.Log(LayerMask.GetMask("Block"));
            RaycastHit2D hit = Physics2D.Raycast(transform.position, targetDirection, Vector2.Distance(transform.position, Target.transform.position), 256);// layer mask = "Block"

            if (hit.collider == null)
            {
                return true;
            }

            return false;
        }
        return false;

    }

    private void Block()
    {
        foreach (Block item in blocks)
        {
            item.Deactivate();
        }

        blocks[skillIndex].Activate();
    }

    public void StopAttack()
    {
        if (attackRoutine != null)
        {
            SpellBook.Instance.StopCasting();
            StopCoroutine(attackRoutine);
            Debug.Log("stop attack");
            IsAttacking = false;
            MyAnimator.SetBool("attack", false);
        }
    }
}
