﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    private static UIManager instance;

    public static UIManager Instance
    {
        get
        {
            if (instance ==null)
            {
                instance = FindObjectOfType<UIManager>();
            }

            return instance;
        }
    }

    [SerializeField]
    private ActionButton[] actionButtons;

    [SerializeField]
    private GameObject enemyFrame;

    private Stat healthStat;

    [SerializeField]
    private CanvasGroup keybindMenu;

    [SerializeField]
    private CanvasGroup spellBook;

    private GameObject[] keybindButtons;

    private void Awake()
    {
        keybindButtons = GameObject.FindGameObjectsWithTag("Keybind");
    }

    // Start is called before the first frame update
    void Start()
    {
        healthStat = enemyFrame.GetComponentInChildren<Stat>();

        //SetUseable(actionButtons[0],SpellBook.Instance.GetSpell("FireBall"));
        //SetUseable(actionButtons[1],SpellBook.Instance.GetSpell("FrostBolt"));
        //SetUseable(actionButtons[2],SpellBook.Instance.GetSpell("ThunderBolt"));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            OpenClose(keybindMenu);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            OpenClose(spellBook);
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            InventoryScript.Instance.OpenClose();
        }

    }

    public void ShowEnemyFrame(NPC target)
    {
        enemyFrame.SetActive(true);
        healthStat.Initialize(target.Health.CurrentValue, target.Health.MaxValue);
        enemyFrame.transform.GetChild(0).GetComponent<Image>().sprite = target.Portrait;

        target.healthChanged += new HealthChanged(UpdateEnemyFrame);

        target.characterRemoved += new CharacterRemoved(HideEnemyFrame);
    }

    public void HideEnemyFrame()
    {
        enemyFrame.SetActive(false);
    }

    public void UpdateEnemyFrame(float value)
    {
        healthStat.CurrentValue = value;
    }

    public void UpdateKeyText(string key,KeyCode code)
    {
        Text tmp = Array.Find(keybindButtons, x => x.name == key).GetComponentInChildren<Text>();
        tmp.text = code.ToString();
    }

    public void ClickActionButton(string buttonName)
    {
        Array.Find(actionButtons, x => x.name == buttonName).MyButton.onClick.Invoke();
    }

    public void OpenClose(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = canvasGroup.alpha > 0 ? 0 : 1;
        canvasGroup.blocksRaycasts = canvasGroup.blocksRaycasts == true ? false : true;
    }

    public void UpdateStackSize(IClickable clickable)
    {
        if (clickable.MyCount > 1)
        {
            clickable.MyStackText.text = clickable.MyCount.ToString();
            clickable.MyStackText.color = Color.white;
            clickable.MyIcon.color = Color.white;
        }
        else
        {
            clickable.MyStackText.color = new Color(0, 0, 0, 0);
        }

        if (clickable.MyCount == 0)
        {
            clickable.MyIcon.color = new Color(0, 0, 0, 0);
            clickable.MyStackText.color = new Color(0, 0, 0, 0);
        }
    }
}
