﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    private Transform map;

    [SerializeField]
    private Texture2D[] mapData;

    [SerializeField]
    private MapElement[] mapElements;

    [SerializeField]
    private Sprite defaultTile;

    private Vector3 WorldStartPos
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(new Vector3(0, 0));
        }
    }

    private Dictionary<Point, GameObject> waterTiles = new Dictionary<Point, GameObject>();

    [SerializeField]
    private SpriteAtlas waterAtlas;

    // Start is called before the first frame update
    void Start()
    {
        GenerateMap();
    }


    //tao map
    private void GenerateMap()
    {

        int height = mapData[0].height;
        int width = mapData[0].width;

        for (int i = 0; i < mapData.Length; i++)
        {
            for (int x = 0; x < mapData[i].width; x++)
            {
                for (int y = 0; y < mapData[i].height; y++)
                {
                    Color c = mapData[i].GetPixel(x, y);

                    MapElement newElement = Array.Find<MapElement>(mapElements, e => e.MyColor == c);

                    if (newElement != null)
                    {
                        float xPos = WorldStartPos.x + (defaultTile.bounds.size.x * x);
                        float yPos = WorldStartPos.y + (defaultTile.bounds.size.y * y);

                        GameObject go = Instantiate(newElement.ElementPrefab);

                        go.transform.position = new Vector2(xPos,yPos);

                        //sorting order layer 
                        if (newElement.TileTag == "Tree")
                        {
                            go.GetComponent<SpriteRenderer>().sortingOrder = height * 2 - y * 2;
                        }

                        //sort water
                        if (newElement.TileTag == "Water")
                        {
                            waterTiles.Add(new Point(x, y), go);
                        }

                        go.transform.parent = map;
                    }
                    else
                        Debug.Log("null map : " + c);

                }
            }
        }

        CheckWarter();
    }

    //check nc
    private void CheckWarter()
    {
        foreach (KeyValuePair<Point, GameObject> tile in waterTiles)
        {
            string composition = TileCheck(tile.Key);

            //thay the
            if (composition[1] == 'E' && composition[3] == 'W' && composition[4] == 'E' && composition[6] == 'W')
            {
                tile.Value.GetComponent<SpriteRenderer>().sprite = waterAtlas.GetSprite("1");
            }

            //chen them map
            if (composition[1] == 'W' && composition[2] == 'E' && composition[4] == 'W')
            {
                GameObject go = Instantiate(tile.Value, tile.Value.transform.position, Quaternion.identity);
                go.GetComponent<SpriteRenderer>().sprite = waterAtlas.GetSprite("38");
                go.GetComponent<SpriteRenderer>().sortingOrder = 1;
                go.transform.parent = map;
            }

            //random nc giua
            if (composition[1] == 'W' && composition[3] == 'W' && composition[4] == 'W' && composition[6] == 'W')
            {
                int randomChance = UnityEngine.Random.Range(0, 100);
                if (randomChance < 15)
                {
                    tile.Value.GetComponent<SpriteRenderer>().sprite = waterAtlas.GetSprite("46");
                }
                else if (randomChance < 30)
                {
                    tile.Value.GetComponent<SpriteRenderer>().sprite = waterAtlas.GetSprite("48");
                }
                else
                {
                    tile.Value.GetComponent<SpriteRenderer>().sprite = waterAtlas.GetSprite("47");
                }
            }

            if (composition[1] == 'W' && composition[2] == 'W' && composition[3] == 'W' && composition[4] == 'W')
            {

            }
        }

    }
    //check nc
    private string TileCheck(Point currentPoint)
    {
        string composition = string.Empty;

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if ( x != 0 || y!= 0)
                {
                    if (waterTiles.ContainsKey(new Point(currentPoint.MyX + x, currentPoint.MyY + y)))
                    {
                        composition += "W";
                    }
                    else
                    {
                        composition += "E";
                    }
                }
            }
        }
        //Debug.Log(composition);

        return composition;
    }
}

[Serializable]
public class MapElement
{
    [SerializeField]
    private string tileTag;

    [SerializeField]
    private Color myColor;

    public Color MyColor { get { return myColor; } }

    [SerializeField]
    private GameObject elementPrefab;

    public GameObject ElementPrefab { get { return elementPrefab; } }

    public string TileTag { get { return tileTag; } }

}

public struct Point
{
    public int MyX { get; set; }

    public int MyY { get; set; }

    public Point(int x,int y)
    {
        this.MyX = x;
        this.MyY = y;
    }
}
