﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BagButton : MonoBehaviour, IPointerClickHandler
{
    private Bag bag;

    [SerializeField]
    private Sprite full, empty;

    public Bag Mybag
    {
        get => bag; set {

            if (value != null)
            {
                GetComponent<Image>().sprite = full;
            }
            else
            {
                GetComponent<Image>().sprite = empty;
            }
            bag = value;
        }
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                HandScript.Instance.TakeMoveable(Mybag);
            }
            else
            if (bag != null)
            {
                bag.MyBagScript.OpenClose();
            }
        }
        
    }

    public void RemoveBag()
    {
        InventoryScript.Instance.RemoveBag(Mybag);
        Mybag.MyBagButton = null;
        Mybag = null;
    }
}
