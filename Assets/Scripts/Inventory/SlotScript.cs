﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotScript : MonoBehaviour, IPointerClickHandler,IClickable
{
    [SerializeField]
    private ObservableStack<Item> items = new ObservableStack<Item>();

    [SerializeField]
    private Image icon;

    public bool IsEmpty
    {
        get
        {
            return items.Count == 0;
        }
    }

    public bool IsFull
    {
        get
        {
            if (IsEmpty || MyCount < MyItem.MyStackSize)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public Item MyItem
    {
        get
        {
            if (!IsEmpty)
            {
                return items.Peek();
            }

            return null;
        }
    }

    public Image MyIcon {
        get => icon;
        set => icon = value;
    }

    [SerializeField]
    private Text stackSize;

    public int MyCount { get => items.Count; }

    public Text MyStackText { get => stackSize; }

    private void Awake()
    {
        items.OnPop += new UpdateStackEvent(UpdateSlot);
        items.OnPush += new UpdateStackEvent(UpdateSlot);
        items.OnClear += new UpdateStackEvent(UpdateSlot);
    }

    public bool AddItem(Item item)
    {
        items.Push(item);
        icon.sprite = item.MyIcon;
        icon.color = Color.white;
        item.MySlot = this;

        return true;
    }

    public bool AddItem(ObservableStack<Item> newItems)
    {
        if (IsEmpty || newItems.Peek().GetType() == MyItem.GetType() )
        {
            int count = newItems.Count;

            for (int i = 0; i < count; i++)
            {
                if (IsFull)
                {
                    return false;
                }
                else
                {
                    AddItem(newItems.Pop());
                }
            }
            return true;
        }

        return false;
    }

    public void RemoveItem(Item item)
    {
        if (!IsEmpty)
        {
            items.Pop();
            //UIManager.Instance.UpdateStackSize(this);
        }
    }

    public void Clear()
    {
        if (items.Count > 0)
        {
            items.Clear();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            //if we don't something to move
            if (InventoryScript.Instance.FromSlot == null && !IsEmpty) 
            {
                HandScript.Instance.TakeMoveable(MyItem as IMoveable);
                InventoryScript.Instance.FromSlot = this;
            }
            else if (InventoryScript.Instance.FromSlot == null && IsEmpty && (HandScript.Instance.MyMoveable is Bag))
            {
                Bag bag = (Bag)HandScript.Instance.MyMoveable;

                AddItem(bag);
                bag.MyBagButton.RemoveBag();

                HandScript.Instance.Drop();
            }
            //if we have some thing to move
            else if(InventoryScript.Instance.FromSlot != null)
            {
                if (PutItemBack()|| MergeItems(InventoryScript.Instance.FromSlot) || SwapItems(InventoryScript.Instance.FromSlot) || AddItem(InventoryScript.Instance.FromSlot.items))
                {
                    HandScript.Instance.Drop();
                    InventoryScript.Instance.FromSlot = null;
                }
            }
        }

        if (eventData.button == PointerEventData.InputButton.Right)
        {
            UseItem();
        }
    }

    public void UseItem()
    {
        if (MyItem is IUseable)
        {
            (MyItem as IUseable).Use();
        }
    }

    public bool StackItem(Item item)
    {
        if (!IsEmpty && item.name == MyItem.name && items.Count < MyItem.MyStackSize)
        {
            items.Push(item);
            item.MySlot = this;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool PutItemBack()
    {
        if (InventoryScript.Instance.FromSlot == this)
        {
            InventoryScript.Instance.FromSlot.MyIcon.color = Color.white;
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool SwapItems(SlotScript from)
    {
        if (IsEmpty)
        {
            return false;
        }

        if (from.MyItem.GetType() != MyItem.GetType() || from.MyCount + MyCount > MyItem.MyStackSize)
        {
            //coppy all items need swap
            ObservableStack<Item> tmpFrom = new ObservableStack<Item>(from.items);

            //clear slot a
            from.items.Clear();
            //all items form slot b and copy them into a
            from.AddItem(items);

            //clear b
            items.Clear();
            //move items form a copy to b
            AddItem(tmpFrom);

            return true;
        }

        return false;
    }

    private bool MergeItems(SlotScript from)
    {
        if (IsEmpty)
        {
            return false;
        }
        if (from.MyItem.GetType() == MyItem.GetType() && !IsFull)
        {
            //how many free slots do wwe have in he stack
            int free = MyItem.MyStackSize - MyCount;

            for (int i = 0; i < free; i++)
            {
                AddItem(from.items.Pop());
            }

            return true;
        }

        return false;
    }



    private void UpdateSlot()
    {
        UIManager.Instance.UpdateStackSize(this);
    }

}
