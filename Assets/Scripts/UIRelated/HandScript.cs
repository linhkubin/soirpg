﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HandScript : MonoBehaviour
{
    private static HandScript instance;

    public static HandScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<HandScript>();
            }

            return instance;
        }
    }


    public IMoveable MyMoveable { get; set; }

    private Image Icon;

    [SerializeField]
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        Icon = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        Icon.transform.position = Input.mousePosition + offset;

        DeleteItem();
    }


    public void TakeMoveable(IMoveable moveable)
    {
        this.MyMoveable = moveable;
        Icon.sprite = moveable.MyIcon;
        Icon.color = Color.white;
    }

    public IMoveable Put()
    {
        IMoveable tmp = MyMoveable;

        MyMoveable = null;

        Icon.color = Color.clear;

        return tmp;
    }

    public void Drop()
    {
        MyMoveable = null;
        Icon.color = Color.clear;

    }

    private void DeleteItem()
    {
        if (Input.GetMouseButton(0)&& !EventSystem.current.IsPointerOverGameObject() && Instance.MyMoveable != null)
        {
            if (MyMoveable is Item && InventoryScript.Instance.FromSlot != null)
            {
                (MyMoveable as Item).MySlot.Clear();
            }

            Drop();

            InventoryScript.Instance.FromSlot = null;
        }
    }
}
