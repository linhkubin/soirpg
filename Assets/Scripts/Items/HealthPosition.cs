﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="HealthPosition",menuName ="Items/Position",order =1)]
public class HealthPosition : Item, IUseable
{
    [SerializeField]
    private int health;

    public void Use()
    {
        Remove();

        Player.Instance.Health.CurrentValue += health;
    }

}
