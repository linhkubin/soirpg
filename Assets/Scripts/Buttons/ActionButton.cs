﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActionButton : MonoBehaviour, IPointerClickHandler
{

    public IUseable MyUseable { get; set; }

    public Button MyButton { get; private set; }

    public Image MyIcon { get => icon; set => icon = value; }

    [SerializeField]
    private Image icon;

    // Start is called before the first frame update
    void Start()
    {
        MyButton = GetComponent<Button>();
        MyButton.onClick.AddListener(OnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        if (MyUseable != null)
        {
            MyUseable.Use();
        }
    }

    //click vao button spell
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (HandScript.Instance.MyMoveable != null && HandScript.Instance.MyMoveable is IUseable)
            {
                SetUseable(HandScript.Instance.MyMoveable as IUseable);
            }
        }
    }

    public void SetUseable(/*ActionButton btn,*/ IUseable useable)
    {
        //btn.MyButton.image.sprite = useable.MyIcon;

        //btn.MyIcon.sprite = useable.MyIcon;
        //btn.MyUseable = useable;

        this.MyUseable = useable;

        UpdateVisual();
    }

    public void UpdateVisual()
    {
        MyIcon.sprite = HandScript.Instance.Put().MyIcon;

        MyIcon.color = Color.white;
    }
}
